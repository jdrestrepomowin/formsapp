import { Component, ViewChild } from '@angular/core';

interface Persona {
  nombre: string;
  favoritos: Favorito[];
};

interface Favorito {
  id: number;
  nombre: string;
}
@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styles: [
  ]
})
export class DinamicosComponent  {
  nombreJuego : string = "";
  persona: Persona = {
    nombre: 'Daniel',
    favoritos: [
      {id: 1, nombre: 'Guitar Hero'},
      {id: 2, nombre: 'Band rock'}
    ]
  }

  guardar(){


  }
  eliminar(id: number){
    this.persona.favoritos.splice(id, 1)
  }

  agregar(){
    const nuevoFavorito: Favorito = {
        id:this.persona.favoritos[this.persona.favoritos.length-1].id + 1,
        nombre:this.nombreJuego
      }
    this.persona.favoritos.push({...nuevoFavorito});
    this.nombreJuego = "";
  }

}
